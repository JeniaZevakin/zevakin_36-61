

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int process(char str[]){
	int k = 0;
	int i = 0;
	while(i + 1 < 256 && str[i + 1] != '\n'){
		i++;
	}
	if(str[i] >= '0' && str[i] <= '9') k=1;
	return k;
}

int main(){
	char str[256];
	FILE * i = fopen("i.txt","r");
	FILE * o = fopen("o.txt","w");
	if (i == NULL){
		fprintf(stderr,"Error open file\n");
		return 1;
	}
	while (fgets(str, 256, i)!= NULL){
		if (process(str)==1)
		fputs(str, o);
	}
	fclose(i);
	fclose(o);
}
