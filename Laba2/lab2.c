#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <mcheck.h>
#include <ctype.h>
/*7
Расположить строки по возрастанию количества цифр 
Входные параметры:
1. Массив 
2. Размерность массива 
Выходные параметры:
1. Количество цифр 
2. Вторая цифра строки */

char** inp_str(int count,int maxlen){
	char buffer[maxlen];
	char **mas_str;  
	mas_str = (char **)malloc(sizeof(char *)*count);
    for (int i = 0; i < count ; i++){
        fgets(buffer, maxlen, stdin);
        mas_str[i] = (char *)malloc(sizeof(char)*strlen(buffer)+1); 
        strcpy(mas_str[i], buffer);                                
    }
    return mas_str; 
}

int count_numbers(char* string){
	int a=0;
	for (int i=0;i<strlen(string)-1;i++){
		if ((isdigit(string[i])))a++;
	}
    return a;
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]);
    }
    free(mas); 
}

void out_str(char* string, int countnumbers, int number){
	printf("Номер: %d Строка: %s Кол-во чисел: %d \n", number, string, countnumbers);
}

int main(){
	mtrace();
	int count = 0;
	int MAXLEN = 0;
	int p = 0;
	printf("Введите кол-во строк:");
	scanf("%d", &count);
	
	printf("Введите максимальную длину:");
	scanf("%d\n", &MAXLEN);
	
	char **mas_str = NULL; 
	mas_str = inp_str(count, MAXLEN);
	printf("Неотсортированный массив\n");
	
	for (int x=0; x<count; x++){
		out_str(mas_str[x], count_numbers(mas_str[x]), x);
	}
	
	for (int i=0;i<count-1;i++){
		for (int k=0;k<count-1;k++){
			int a=0;
			int b=0;
			a = count_numbers(mas_str[k]);
			b = count_numbers(mas_str[k+1]);
			if(a<b){
				char* t;    
				t = mas_str[k];
				mas_str[k] = mas_str[k+1];     
				mas_str[k+1] = t;             
				p++;	
			}
		}
	}
	
	printf("Отсортированный массив Количество перестановок:%d\n", p);
	
	for (int x=0; x<count; x++){
		out_str(mas_str[x], count_numbers(mas_str[x]), x);
	}	
	
	freeMas(mas_str, count);	
	return 0;
}
